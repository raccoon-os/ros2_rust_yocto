#!/bin/sh

### BEGIN INIT INFO
# Provides:          copy-opkg-state
# Required-Start:    $remote_fs
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: Copy OPKG state on boot
# Description:       Copy OPKG state from /usr/lib/opkg to /var/lib/opkg on system boot
### END INIT INFO

# Path to store the previous OSTree deployment
PREV_DEPLOYMENT_FILE="/var/prev_ostree_deployment"

# Path to the current OSTree deployment
CURRENT_DEPLOYMENT_FILE="/var/sota/import/installed_versions"

# Function to copy OPKG state
copy_opkg_state() {
    echo "Copying OPKG state from /usr/lib/opkg to /var/lib/opkg"
    cp -r /usr/lib/opkg /var/lib/
    rm -rf /var/cache/opkg
}

case "$1" in
  start)
    # Read current deployment
    current_deployment=$(cat "$CURRENT_DEPLOYMENT_FILE")

    # Check if previous deployment file exists
    if [ -f "$PREV_DEPLOYMENT_FILE" ]; then
        prev_deployment=$(cat "$PREV_DEPLOYMENT_FILE")
        
        # Compare current and previous deployments
        if [ "$current_deployment" != "$prev_deployment" ]; then
            echo "New OSTree deployment detected. Copying OPKG state."
            copy_opkg_state
        else
            echo "No new deployment. OPKG state remains unchanged."
        fi
    else
        echo "No previous deployment file found. Copying OPKG state."
        copy_opkg_state
    fi

    # Update the previous deployment file
    echo "$current_deployment" > "$PREV_DEPLOYMENT_FILE"
    ;;
  stop)
    # Nothing to do on stop
    ;;
  restart|reload)
    # Nothing to do on restart or reload
    ;;
  *)
    echo "Usage: $0 {start}"
    exit 1
    ;;
esac

exit 0