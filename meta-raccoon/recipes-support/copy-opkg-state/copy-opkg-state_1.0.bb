SUMMARY = "Copy OPKG State Script"
DESCRIPTION = "Script to copy OPKG state from /usr/lib to /var/lib on OSTree upgrades"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://copy-opkg-state.sh"

inherit update-rc.d

INITSCRIPT_NAME = "copy-opkg-state"
INITSCRIPT_PARAMS = "start 36 2 3 4 5 ."

do_install() {
    install -d ${D}${sysconfdir}/init.d
    install -m 0755 ${WORKDIR}/copy-opkg-state.sh ${D}${sysconfdir}/init.d/copy-opkg-state
}

FILES_${PN} += "${sysconfdir}/init.d/copy-opkg-state"