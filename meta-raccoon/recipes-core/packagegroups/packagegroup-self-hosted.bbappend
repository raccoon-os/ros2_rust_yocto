# Remove X11 and OpenGL requirements
REQUIRED_DISTRO_FEATURES:remove = "x11"
REQUIRED_DISTRO_FEATURES:remove = "opengl"

# Remove graphics package group
PACKAGES:remove = "packagegroup-self-hosted-graphics"
RDEPENDS:packagegroup-self-hosted:remove = "packagegroup-self-hosted-graphics"

# Remove settings-daemon which depends on x11
RDEPENDS:packagegroup-self-hosted-extended:remove = "settings-daemon"
