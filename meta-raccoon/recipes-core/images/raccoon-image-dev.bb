SUMMARY = "The base image used for development on RACCOON OS and applications"
LICENSE = "GPLv2"

inherit core-image

IMAGE_FEATURES += "debug-tweaks ssh-server-openssh"
IMAGE_FEATURES += "package-management"
IMAGE_OVERHEAD_FACTOR = "1.1"

IMAGE_INSTALL += " \
    ostree-booted \
    copy-opkg-state \
    connman \
    connman-client \
    wget \
    ntp \
    btrfs-tools \
"