SUMMARY = "RACCOON ROS2 SDK Image"
DESCRIPTION = "${SUMMARY}"

inherit core-image
inherit ros_distro_${ROS_DISTRO}
inherit ${ROS_DISTRO_TYPE}_image

IMAGE_INSTALL:append = " \
    packagegroup-core-boot \
    ${CORE_IMAGE_EXTRA_INSTALL} \
"

inherit core-image
inherit ros_distro_${ROS_DISTRO}
inherit ${ROS_DISTRO_TYPE}_image

# opencv-staticdev
TOOLCHAIN_TARGET_TASK:append = " \
    python3-numpy-staticdev \
    libeigen \
    eigen3-cmake-module \
    boost \
    libstdc++-staticdev \
    pcl-dev \
    python3-pykdl \
    bullet \
    qhull-staticdev \
    orocos-kdl \
    yaml-cpp-vendor \
    pybind11-vendor \
    rttest \
    tlsf-staticdev \
    tlsf-cpp \
    python-cmake-module \
    python3-opencv \
    tinyxml-vendor \
"
TOOLCHAIN_HOST_TASK:append = "\
    nativesdk-action-msgs \
    nativesdk-builtin-interfaces \
    nativesdk-rcl-interfaces \
    nativesdk-std-msgs \
    nativesdk-unique-identifier-msgs \
    nativesdk-rcl \
    nativesdk-rcl-logging-spdlog \
    nativesdk-rcl-yaml-param-parser \
    nativesdk-rmw-implementation \
    nativesdk-rmw-implementation-cmake \
    nativesdk-rmw-fastrtps-cpp \
    nativesdk-rmw-fastrtps-shared-cpp \
    nativesdk-rosidl-typesupport-interface \
    nativesdk-rosidl-typesupport-c \
    nativesdk-rosidl-typesupport-cpp \
    nativesdk-rosidl-typesupport-introspection-c \
    nativesdk-rosidl-typesupport-introspection-cpp \
    nativesdk-tracetools \
    nativesdk-ament-index-cpp \
    nativesdk-rcpputils \
    nativesdk-rcl-logging-interface \
"
