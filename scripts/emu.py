#!/usr/bin/env python3

import argparse
import os
import subprocess
import sys
import urllib.request
import time
import shutil

def download_file(url, filename):
    if url.startswith("/"):
        print(f"Copying {url} to {filename}.")
        shutil.copyfile(url, filename)
        return

    print(f"Downloading {filename} from {url}")
    urllib.request.urlretrieve(url, filename)

def create_and_resize_overlay(base_image, overlay_size):
    overlay_image = f"{os.path.splitext(base_image)[0]}-overlay.qcow2"
    if not os.path.exists(overlay_image):
        print(f"Creating overlay image of size {overlay_size}")
        subprocess.run(["qemu-img", "create", "-f", "qcow2", "-F", "raw", "-b", base_image, overlay_image, overlay_size], check=True)
    else:
        print("Overlay image already exists. Skipping creation and resizing.")
    return overlay_image

def main():
    parser = argparse.ArgumentParser(description="Run QEMU with Yocto image")
    parser.add_argument("-i", "--image", required=True, help="Image name")
    parser.add_argument("-p", "--path", required=True, help="Deployment path")
    parser.add_argument("-o", "--overlay-size", default="10G", help="Overlay size")
    parser.add_argument("-u", "--download-url", default="https://raccoon.jdiez.me/deploy", help="Download URL")
    args = parser.parse_args()

    if not args.download_url.startswith("http"):
        args.download_url = os.path.abspath(args.download_url)

    os.makedirs(args.path, exist_ok=True)
    os.chdir(args.path)

    uboot_rom = "u-boot-qemux86-64.rom"
    rootfs_image = f"{args.image}-qemux86-64.rootfs.ota-btrfs"

    for file in [uboot_rom, rootfs_image]:
        if not os.path.exists(file):
            download_file(f"{args.download_url}/images/qemux86-64/{file}", file)

    overlay_image = create_and_resize_overlay(rootfs_image, args.overlay_size)

    qemu_command = [
        "qemu-system-x86_64",
        "-bios", uboot_rom,
        "-serial", "stdio",
        "-m", "1G",
        "-object", "rng-random,id=rng0,filename=/dev/urandom",
        "-device", "virtio-rng-pci,rng=rng0",
        "-netdev", "user,id=raccoon0,hostfwd=tcp::2221-:22",
        "-device", "e1000,netdev=raccoon0,mac=ca:fe:96:d8:aa:7d",
        "-nographic",
        "-monitor", "null",
        "-cpu", "Haswell",
        overlay_image
    ]

    subprocess.run(qemu_command, check=True)

if __name__ == "__main__":
    main()
