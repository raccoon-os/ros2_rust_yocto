#!/bin/bash

# This script is intended to be run from the root directory of this repository.
# It builds all packages in the kas/repo.yml file for all supported archictectures,
# and generates a package index.

# Supported presets
presets=("imx8mp" "qemux86")

export KAS_BASE_CONFIG="kas/full.yml:kas/repo.yml"

build_packages() {
    local preset=$1
    local kas_preset_config="kas/preset/$preset.yml:$KAS_BASE_CONFIG"

    echo "Building packages for preset: $preset"

    time (kas build $kas_preset_config -- --runall=do_package_write_ipk \
        > build/tmp/log/repo-$preset.txt 2>&1)
    if [ $? -ne 0 ]; then
        echo "Error: repo build for $preset failed. Please check the log file: build/tmp/log/repo-$preset.txt"
        return 1
    fi

    kas build $kas_preset_config --target package-index \
        > build/tmp/log/repo-pkgindex-$preset.txt 2>&1

    if [ $? -ne 0 ]; then
        echo "Error: package index for $preset failed. Please check the log file: build/tmp/log/repo-pkgindex-$preset.txt"
        return 1
    fi
}

echo "Starting repo build process for all presets"

for preset in "${presets[@]}"; do
    build_packages "$preset"
done

echo "Repo build complete."
