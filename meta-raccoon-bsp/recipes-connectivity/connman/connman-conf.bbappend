# This bbappend removes the main.conf file to enable auto eth connectivity on qemu machines
do_install:append:qemuall() {
    if [ -f ${D}${sysconfdir}/connman/main.conf ]; then
        rm ${D}${sysconfdir}/connman/main.conf
    fi
}