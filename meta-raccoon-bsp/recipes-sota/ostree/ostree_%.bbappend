# We use ostree-prepare-root inside an initramfs, so we don't want to have the 
# static version of ostree-prepere-root which assumes it is running as PID 1
PACKAGECONFIG:remove = "static"