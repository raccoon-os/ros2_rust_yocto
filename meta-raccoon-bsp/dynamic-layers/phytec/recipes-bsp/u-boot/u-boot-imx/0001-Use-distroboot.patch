From d620790df5e9f7aa5d20ba620cf5d4fd470d5158 Mon Sep 17 00:00:00 2001
From: Jose Manuel Diez <j.diezlopez@tu-berlin.de>
Date: Sat, 10 Aug 2024 16:30:00 +0000
Subject: [PATCH] Use distroboot

---
 .../phytec/phycore_imx8mp/phycore_imx8mp.env  |  77 -------------
 configs/phycore-imx8mp_defconfig              |   1 +
 include/configs/phycore_imx8mp.h              | 103 ++++++++++++++++++
 3 files changed, 104 insertions(+), 77 deletions(-)
 delete mode 100644 board/phytec/phycore_imx8mp/phycore_imx8mp.env

diff --git a/board/phytec/phycore_imx8mp/phycore_imx8mp.env b/board/phytec/phycore_imx8mp/phycore_imx8mp.env
deleted file mode 100644
index 81419ec720b..00000000000
--- a/board/phytec/phycore_imx8mp/phycore_imx8mp.env
+++ /dev/null
@@ -1,77 +0,0 @@
-#include <env/phytec/rauc.env>
-#include <env/phytec/overlays.env>
-
-bootcmd=
-	if test ${dofastboot} = 1; then
-		fastboot 0;
-	fi;
-	mmc dev ${mmcdev};
-	if mmc rescan; then
-		if test ${doraucboot} = 1; then
-			run raucinit;
-		fi;
-		if run loadimage; then
-			run mmcboot;
-		else
-			run netboot;
-		fi;
-	fi;
-console=ttymxc0,115200
-bootenv_addr_r=0x49100000
-fdtoverlay_addr_r=0x49000000
-dofastboot=0
-emmc_dev=2
-fastboot_raw_partition_all=0 4194304
-fastboot_raw_partition_bootloader=64 8128
-fdt_addr_r=0x48000000
-fdtfile=CONFIG_DEFAULT_FDT_FILE
-image=Image
-ip_dyn=yes
-loadimage=fatload mmc ${mmcdev}:${mmcpart} ${loadaddr} ${image}
-loadfdt=fatload mmc ${mmcdev}:${mmcpart} ${fdt_addr_r} ${fdtfile}
-mmcargs=
-	setenv bootargs console=${console}
-	root=/dev/mmcblk${mmcdev}p${mmcroot} ${raucargs} rootwait rw
-mmcautodetect=yes
-mmcboot=
-	echo Booting from mmc ...;
-	if test ${no_bootenv} = 0; then
-		if run mmc_load_bootenv; then
-			env import -t ${bootenv_addr_r} ${filesize};
-		fi;
-	fi;
-	run mmcargs;
-	if run loadfdt; then
-		run mmc_apply_overlays;
-		booti ${loadaddr} - ${fdt_addr_r};
-	else
-		echo WARN: Cannot load the DT;
-	fi;
-mmcdev=CONFIG_SYS_MMC_ENV_DEV
-mmcpart=1
-mmcroot=2
-netargs=
-	setenv bootargs console=${console} root=/dev/nfs ip=dhcp
-	nfsroot=${serverip}:${nfsroot},v3,tcp
-netboot=
-	echo Booting from net ...;
-	if test ${ip_dyn} = yes; then
-		setenv get_cmd dhcp;
-	else
-		setenv get_cmd tftp;
-	fi;
-	if test ${no_bootenv} = 0; then
-		if run net_load_bootenv; then
-			env import -t ${bootenv_addr_r} ${filesize};
-		fi;
-	fi;
-	${get_cmd} ${loadaddr} ${image};
-	run netargs;
-	if ${get_cmd} ${fdt_addr_r} ${fdtfile}; then
-		run net_apply_overlays;
-		booti ${loadaddr} - ${fdt_addr_r};
-	else
-		echo WARN: Cannot load the DT;
-	fi;
-nfsroot=/srv/nfs
-sd_dev=1
diff --git a/configs/phycore-imx8mp_defconfig b/configs/phycore-imx8mp_defconfig
index 124c38726b4..677cbf92ef8 100644
--- a/configs/phycore-imx8mp_defconfig
+++ b/configs/phycore-imx8mp_defconfig
@@ -1,3 +1,4 @@
+CONFIG_DISTRO_DEFAULTS=y
 CONFIG_ARM=y
 CONFIG_ARCH_IMX8M=y
 CONFIG_TEXT_BASE=0x40200000
diff --git a/include/configs/phycore_imx8mp.h b/include/configs/phycore_imx8mp.h
index 299fabc6a99..12ce303e04d 100644
--- a/include/configs/phycore_imx8mp.h
+++ b/include/configs/phycore_imx8mp.h
@@ -9,10 +9,113 @@
 
 #include <linux/sizes.h>
 #include <asm/arch/imx-regs.h>
+#include "imx_env.h"
 
 #define CFG_SYS_UBOOT_BASE \
 		(QSPI0_AMBA_BASE + CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR * 512)
 
+#define BOOT_TARGET_DEVICES(func) \
+    func(MMC, mmc, 1) \
+    func(MMC, mmc, 2) \
+    func(DHCP, dhcp, na)
+#include <config_distro_bootcmd.h>
+
+#define JAILHOUSE_ENV \
+	"jh_clk= \0 " \
+	"jh_mmcboot=setenv fdtfile imx8mq-evk-root.dtb; " \
+		"setenv jh_clk clk_ignore_unused mem=1872M; " \
+			  "if run loadimage; then " \
+				  "run mmcboot; " \
+			  "else run jh_netboot; fi; \0" \
+	"jh_netboot=setenv fdtfile imx8mq-evk-root.dtb; setenv jh_clk clk_ignore_unused mem=1872MB; run netboot; \0 "
+
+#define SR_IR_V2_COMMAND \
+	"nodes=/soc@0/caam-sm@100000 /soc@0/bus@30000000/caam_secvio /soc@0/bus@30000000/caam-snvs@30370000 /soc@0/bus@32c00000/hdmi@32c00000 /soc@0/bus@32c00000/display-controller@32e00000 /soc@0/vpu@38300000 /soc@0/vpu_v4l2 /gpu3d@38000000 /audio-codec-bt-sco /audio-codec /sound-bt-sco /sound-wm8524 /sound-spdif /sound-hdmi-arc /binman \0" \
+	"sr_ir_v2_cmd=cp.b ${fdtcontroladdr} ${fdt_addr_r} 0x10000;"\
+	"fdt addr ${fdt_addr_r};"\
+	"fdt set /soc@0/usb@38100000 compatible snps,dwc3;" \
+	"fdt set /soc@0/usb@38200000 compatible snps,dwc3;" \
+	"for i in ${nodes}; do fdt rm ${i}; done \0"
+
+#define CFG_MFG_ENV_SETTINGS \
+	CFG_MFG_ENV_SETTINGS_DEFAULT \
+	"initrd_addr=0x43800000\0" \
+	"initrd_high=0xffffffffffffffff\0" \
+	"emmc_dev=0\0"\
+	"sd_dev=1\0"
+
+/* Initial environment variables */
+#define CFG_EXTRA_ENV_SETTINGS		\
+	CFG_MFG_ENV_SETTINGS \
+	BOOTENV \
+	SR_IR_V2_COMMAND \
+	JAILHOUSE_ENV \
+	"prepare_mcore=setenv mcore_clk clk-imx8mq.mcore_booted;\0" \
+	"scriptaddr=0x43500000\0" \
+	"kernel_addr_r=" __stringify(CONFIG_SYS_LOAD_ADDR) "\0" \
+	"bsp_script=boot.scr\0" \
+	"image=Image\0" \
+	"splashimage=0x50000000\0" \
+	"console=ttymxc0,115200\0" \
+	"fdt_addr_r=0x43000000\0"			\
+	"fdt_addr=0x43000000\0"			\
+	"fdt_high=0xffffffffffffffff\0"		\
+	"boot_fdt=try\0" \
+	"fdtfile=imx8mp-phyboard-pollux-rdk.dtb\0" \
+	"bootm_size=0x10000000\0" \
+	"mmcdev="__stringify(CONFIG_SYS_MMC_ENV_DEV)"\0" \
+	"mmcpart=1\0" \
+	"mmcroot=/dev/mmcblk1p2 rootwait rw\0" \
+	"mmcautodetect=yes\0" \
+	"mmcargs=setenv bootargs ${jh_clk} ${mcore_clk} console=${console} root=${mmcroot}\0 " \
+	"loadbootscript=fatload mmc ${mmcdev}:${mmcpart} ${loadaddr} ${bsp_script};\0" \
+	"bootscript=echo Running bootscript from mmc ...; " \
+		"source\0" \
+	"loadimage=fatload mmc ${mmcdev}:${mmcpart} ${loadaddr} ${image}\0" \
+	"loadfdt=fatload mmc ${mmcdev}:${mmcpart} ${fdt_addr_r} ${fdtfile}\0" \
+	"mmcboot=echo Booting from mmc ...; " \
+		"run mmcargs; " \
+		"if test ${boot_fdt} = yes || test ${boot_fdt} = try; then " \
+			"if run loadfdt; then " \
+				"booti ${loadaddr} - ${fdt_addr_r}; " \
+			"else " \
+				"echo WARN: Cannot load the DT; " \
+			"fi; " \
+		"else " \
+			"echo wait for boot; " \
+		"fi;\0" \
+	"netargs=setenv bootargs ${jh_clk} ${mcore_clk} console=${console} " \
+		"root=/dev/nfs " \
+		"ip=dhcp nfsroot=${serverip}:${nfsroot},v3,tcp\0" \
+	"netboot=echo Booting from net ...; " \
+		"run netargs;  " \
+		"if test ${ip_dyn} = yes; then " \
+			"setenv get_cmd dhcp; " \
+		"else " \
+			"setenv get_cmd tftp; " \
+		"fi; " \
+		"${get_cmd} ${loadaddr} ${image}; " \
+		"if test ${boot_fdt} = yes || test ${boot_fdt} = try; then " \
+			"if ${get_cmd} ${fdt_addr_r} ${fdtfile}; then " \
+				"booti ${loadaddr} - ${fdt_addr_r}; " \
+			"else " \
+				"echo WARN: Cannot load the DT; " \
+			"fi; " \
+		"else " \
+			"booti; " \
+		"fi;\0" \
+	"bsp_bootcmd=echo Running BSP bootcmd ...; " \
+			"mmc dev ${mmcdev}; if mmc rescan; then " \
+			  "if run loadbootscript; then " \
+				  "run bootscript; " \
+			  "else " \
+				  "if run loadimage; then " \
+					  "run mmcboot; " \
+				  "else run netboot; " \
+				  "fi; " \
+			  "fi; " \
+		  "fi;"
+
 /* Link Definitions */
 
 #define CFG_SYS_INIT_RAM_ADDR	0x40000000
-- 
2.34.1

