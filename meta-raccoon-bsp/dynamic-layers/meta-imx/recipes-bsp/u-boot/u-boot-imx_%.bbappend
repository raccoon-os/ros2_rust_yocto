FILESEXTRAPATHS:prepend = "${RACCOON_BSP_TOPDIR}/recipes-bsp/u-boot/files:"

SRC_URI:append:sota = " \
	file://boot.cmd \
	file://uEnv-ostree.txt \
"

# Add btrfs support in u-boot
FILESEXTRAPATHS:prepend = "${RACCOON_BSP_TOPDIR}/recipes-bsp/u-boot/fragments:"
SRC_URI:append:sota = " file://btrfs.cfg"

do_deploy:append:sota() {
	install -m 0755 ${WORKDIR}/uEnv-ostree.txt ${DEPLOYDIR}/uEnv.txt
}

# The UBOOT_ENV_SUFFIX and UBOOT_ENV are mandatory in order to run the
# uboot-mkimage command from poky/meta/recipes-bsp/u-boot/u-boot.inc
UBOOT_ENV_SUFFIX = "scr"
UBOOT_ENV:sota = "boot"

COMPATIBLE_MACHINE = "(phyboard-pollux-imx8mp-3)"
