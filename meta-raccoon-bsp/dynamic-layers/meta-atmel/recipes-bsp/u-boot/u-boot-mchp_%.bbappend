# Don't build the default Atmel target,
# we want to use a merged defconfig instead.
# This hack is needed because Atmel decided to use a DIY u-boot
# recipe instead of the perfectly good one available in poky.
# The recipe in poky supports things like config fragments, 
# which is useful in many situations (like here, we want to add
# support for btrfs), while Atmel's does not.
do_compile () {
	if [ "${@bb.utils.contains('DISTRO_FEATURES', 'ld-is-gold', 'ld-is-gold', '', d)}" = "ld-is-gold" ] ; then
		sed -i 's/$(CROSS_COMPILE)ld$/$(CROSS_COMPILE)ld.bfd/g' config.mk
	fi

	unset LDFLAGS
	unset CFLAGS
	unset CPPFLAGS

	if [ ! -e ${B}/.scmversion -a ! -e ${S}/.scmversion ]
	then
		echo ${UBOOT_LOCALVERSION} > ${B}/.scmversion
		echo ${UBOOT_LOCALVERSION} > ${S}/.scmversion
	fi

    # This line is commented because of the reasons described above.
    # It's the only line that is different from the do_compile() function in meta-atmel/r-bsp/u-boot
	#oe_runmake ${UBOOT_MACHINE}
	oe_runmake ${UBOOT_MAKE_TARGET}
}

# Include the do_configure() function from the poky recipe
BBPATH .= ":${COREBASE}/meta/recipes-bsp/u-boot"
require u-boot-configure.inc

# Finally, add btrfs support in u-boot
FILESEXTRAPATHS:prepend = "${RACCOON_BSP_TOPDIR}/recipes-bsp/u-boot/fragments:"
SRC_URI:append:sota = " file://btrfs.cfg"
