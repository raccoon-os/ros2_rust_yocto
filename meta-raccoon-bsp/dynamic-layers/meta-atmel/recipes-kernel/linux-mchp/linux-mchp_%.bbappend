require ${@bb.utils.contains('DISTRO_FEATURES', 'sota', '${BPN}_sota.inc', '', d)}

# Add support for custom device tree for the Quantum CyBEE OBC

FILESEXTRAPATHS:append := "${THISDIR}/files:"
SRC_URI:append = " file://quantum-cybee.dts"

do_configure:append() {
    cp "${WORKDIR}/quantum-cybee.dts" "${S}/arch/arm/boot/dts/microchip"
    echo "dtb-$(CONFIG_SOC_SAM_V7) += quantum-cybee.dtb" >> ${S}/arch/arm/boot/dts/microchip/Makefile
}