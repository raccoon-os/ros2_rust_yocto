SOTA_MACHINE:phyboard-pollux-imx8mp-3 ?= "phyboard-pollux-imx8mp-3"
SOTA_MACHINE:sama5d27-som1-ek-sd ?= "sama5d27-som1-ek-sd"

WKS_FILE:sota ?= "sdimage-sota-btrfs.wks"

inherit sota

IMAGE_FSTYPES:remove:sota = "ota-ext4"
IMAGE_FSTYPES += "ota-btrfs"

OSTREE_OSNAME = "raccoon-os"