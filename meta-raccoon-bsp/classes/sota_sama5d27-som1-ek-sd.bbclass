KERNEL_IMAGETYPES += "fitImage" 
KERNEL_CLASSES += "kernel-fitimage"

#UBOOT_RD_LOADADDRESS = "0x50000000"
#UBOOT_RD_ENTRYPOINT = "0x50000000"
#UBOOT_DTB_LOADADDRESS = "0x23000000"

OSTREE_KERNEL = "fitImage-${INITRAMFS_IMAGE}-${MACHINE}-${MACHINE}"
OSTREE_KERNEL_ARGS = "console=ttyS0,115200 ramdisk_size=8192 root=/dev/ram0 rw rootfstype=ext4 ostree_root=/dev/mmcblk0p2"

MACHINE_ESSENTIAL_EXTRA_RDEPENDS:remove = "dt-overlay-mchp"
KERNEL_DEVICETREE := "quantum-cybee.dtb"

WKS_FILES:sama5d27-som1-ek-sd := "sdimage-sota-btrfs.wks" 
IMAGE_BOOT_FILES:append = " boot.scr uEnv.txt" 

# Don't copy Atmel's default boot commands/uboot environment
IMAGE_BOOT_FILES:remove = "uboot.env" 
IMAGE_BOOT_FILES:remove = "sama5d27_som1_ek.itb"

# Copy RACCOON u-boot script
do_image_wic[depends] += "raccoon-ota-bootscript:do_deploy "