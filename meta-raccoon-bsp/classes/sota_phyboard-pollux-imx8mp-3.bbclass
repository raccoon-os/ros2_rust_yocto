# TODO: it seems fitImages are already enabled by default in phyimx8m.inc
#KERNEL_IMAGETYPES += "Image fitImage" 
#KERNEL_CLASSES += "kernel-fitimage"

#UBOOT_LOADADDRESS = "0x48000000"
#UBOOT_ENTRYPOINT:phyboard-pollux-imx8mp-3 = "0x48000000"
UBOOT_RD_LOADADDRESS = "0x50000000"
UBOOT_RD_ENTRYPOINT = "0x50000000"
UBOOT_DTB_LOADADDRESS = "0x4A300000"

OSTREE_KERNEL = "fitImage-${INITRAMFS_IMAGE}-${MACHINE}-${MACHINE}"
OSTREE_KERNEL_ARGS = "console=ttymxc0,115200 ramdisk_size=8192 root=/dev/ram0 rw rootfstype=ext4 ostree_root=/dev/mmcblk1p2"

# we want to use ostree-prepare-sysroot as pid1
#INITRAMFS_IMAGE:phyboard-pollux-imx8mp-3 = ""

WKS_FILES:phyboard-pollux-imx8mp-3 := "imx8m-sdimage-ota.wks" 
IMAGE_BOOT_FILES:append = " boot.scr uEnv.txt" 