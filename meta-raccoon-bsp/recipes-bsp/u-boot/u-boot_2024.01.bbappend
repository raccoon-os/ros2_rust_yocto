FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

# This patch is only really needed for raspberrypi-style 
# device tree overlays (see https://github.com/raspberrypi/firmware/issues/1718)
# but it may be useful for raccoon-supported device tree overlays later on
# so apply it to the base u-boot recipe.
SRC_URI += "file://0001-libfdt-overlay-Skip-phandles-not-in-subnodes.patch"