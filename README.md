<p float="left">
    <img src="graphics/raccoon_logo.svg" width="45%" />
    <img src="graphics/partner_logos.svg" width="45%" />
</p>

# RACCOON OS

RACCOON OS is a distribution of Linux based on the [Yocto Project](https://www.yoctoproject.org/) intended for space applications.
It is currently in a very early stage of development.
The documentation is available [here](https://docs.raccoon.jdiez.me/) ([source code](https://gitlab.com/raccoon-os/docs))

# Get Involved

We're still getting the project up and running, but you are *very welcome* to join our weekly meeting, on Wednesdays at 3PM CET (13:00 UTC) [here](https://tu-berlin.zoom.us/j/66167262545?pwd=d0t5aUlHNDhaUmc1a3lTL0hRZmdhUT09).

# Quick start

We use [kas](https://github.com/siemens/kas) to manage the Yocto build configuration and environment setup.

1. Clone this repository
 - `git clone git@gitlab.com:raccoon-os/raccoon-os.git`
2. Install `kas`
 - `pip install kas`
3. Build the image
 - `kas-container build config.yml`
 - This step can take many hours, depending on your build machine.
 - This will be sped up once sstate cache is published.
4. Run the `qemuarm64` emulator
 - `kas-container shell config.yml`
 - `runqemu qemuarm64 slirp nographic`

# Release TODOs

- [ ] Publish sstate cache
- [ ] Add `LICENSE` file
- [ ] Document what is currently available in the image
- [ ] Document the process of adding ROS2 workspaces.

